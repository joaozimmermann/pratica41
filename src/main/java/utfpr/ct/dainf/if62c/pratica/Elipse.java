package utfpr.ct.dainf.if62c.pratica;

public class Elipse {
    private double r;
    private double s;

    public Elipse(double eixo1, double eixo2) {
        this.r = eixo1;
        this.s = eixo2;
    }
    
    public double getArea(){
        double a = Math.PI*r*s;
        return a;
    }
    
    public double getPerimetro(){
        double a = Math.PI*((3*(r+s))-Math.sqrt(((3*r+s)*(r+3*s))));
        return a;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getR() {
        return r;
    }

    public double getS() {
        return s;
    }
    
}
