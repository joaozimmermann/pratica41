
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

public class Pratica41 {
    public static void main(String[] args) {
        Elipse teste = new Elipse(2, 4);
        Circulo teste2 = new Circulo(2);
        System.out.println(teste.getPerimetro());
        System.out.println(teste.getArea());
        System.out.println(teste2.getPerimetro());
    }
} 
